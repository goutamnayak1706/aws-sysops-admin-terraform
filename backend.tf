# #This is for creating a terraform backend which will store the TF state file in the S3 bucket which we have created earlier

terraform {
  backend "s3" {
    bucket = "terraform-state-storage-s3-gou"
    key    = "terraform/cpp/terraform_dev.tfstate"
    region = "eu-west-1"
    dynamodb_table = "terraform-state-lock-dynamo-gou"
    #encrypt = true # Optional, S3 Bucket Server Side Encryption
  }
}