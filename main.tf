# Pre-Requisites creation - Created in c:\Test\main.tf
# S3 bucket and an Dynamo DB table has been created
#If the state file is stored remotely so that many people can access it, then you risk multiple people attempting to make changes to the same file at the exact same time. 
#So we need to provide a mechanism that will “lock” the state if its currently in-use by another user. 
#We can accomplish this by creating a dynamoDB table for terraform to use.

### Chapter 3 ###
# Create Users
resource "aws_iam_user" "user1" {
  name = "alice"
  path = "/"  
}


resource "aws_iam_user" "user2" {
  name = "bob"
  path = "/"  
}


resource "aws_iam_user" "user3" {
  name = "charlie"
  path = "/"  
}

#Assign Keys
resource "aws_iam_access_key" "lb" {
  user = "${aws_iam_user.user2.name}"  
}

#Create IAM Groups
resource "aws_iam_group" "admins" {
  name = "admins"
  path = "/"
}
resource "aws_iam_group" "monitors" {
  name = "monitors"
  path = "/"
}

resource "aws_iam_group" "operators" {
  name = "operators"
  path = "/"
}

resource "aws_iam_user_group_membership" "user1" {
  user = "${aws_iam_user.user1.name}"

  groups = [
    "${aws_iam_group.admins.name}"   
  ]
}

resource "aws_iam_user_group_membership" "user2" {
  user = "${aws_iam_user.user2.name}"

  groups = [
    "${aws_iam_group.monitors.name}"
  ]
}

resource "aws_iam_user_group_membership" "user3" {
  user = "${aws_iam_user.user3.name}"

  groups = [
    "${aws_iam_group.operators.name}"
  ]
}

resource "aws_iam_group_policy" "admins_policy" {
  name  = "admins_policy"
  group = "${aws_iam_group.admins.id}"
   policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}
EOF
}

data "aws_iam_policy_document" "example" {
    statement{       
            actions = [
                "a4b:Get*",
                "a4b:List*",
                "a4b:Describe*",
                "a4b:Search*",
                "acm:Describe*",                
                "lightsail:GetRelationalDatabaseSnapshots",
                "lightsail:GetResources",
                "lightsail:GetStaticIp",
                "lightsail:GetStaticIps",
                "lightsail:GetTagKeys",
                "lightsail:GetTagValues",
                "lightsail:Is*",
                "lightsail:List*",
                "logs:Describe*",
                "logs:Get*",
                "logs:FilterLogEvents",
                "logs:ListTagsLogGroup",
                "logs:StartQuery",
                "logs:TestMetricFilter",
                "machinelearning:Describe*",
                "machinelearning:Get*",
                "mgh:Describe*"                
            ],
            effect = "Allow",
            resources= ["*"]
        }
    }


resource "aws_iam_group_policy" "monitors_policy" {
  name  = "monitors_policy"
  group = "${aws_iam_group.monitors.id}",
  policy = "${data.aws_iam_policy_document.example.json}"
}


resource "aws_iam_group_policy" "operators_policy" {
  name  = "operators_policy"
  group = "${aws_iam_group.operators.id}"
   policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "ec2:*",
            "Effect": "Allow",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "elasticloadbalancing:*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "cloudwatch:*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "autoscaling:*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:CreateServiceLinkedRole",
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "iam:AWSServiceName": [
                        "autoscaling.amazonaws.com",
                        "ec2scheduled.amazonaws.com",
                        "elasticloadbalancing.amazonaws.com",
                        "spot.amazonaws.com",
                        "spotfleet.amazonaws.com",
                        "transitgateway.amazonaws.com"
                    ]
                }
            }
        }
    ]
}
EOF
}

resource "aws_iam_role" "bastionhost" {
  name = "bastionhost"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["ec2.amazonaws.com","s3.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    tag-key = "tag-value"
  }
}





# resource "aws_iam_user_policy" "lb_ro" {
#   name = "test"
#   user = "${aws_iam_user.lb.name}"

#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": [
#         "ec2:Describe*"
#       ],
#       "Effect": "Allow",
#       "Resource": "*"
#     }
#   ]
# }
# EOF
# }



#########          Excercise 4.1                     ######

data "aws_ami" "amazon_linux2" {
  most_recent = true
  owners=["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}

resource "aws_instance" "ec2_instance" {
  ami           = "${data.aws_ami.amazon_linux2.id}"
  instance_type = "t2.micro"
  key_name      = "GoutamKP"
  security_groups = ["${aws_security_group.gou_sec_grp.name}"]
  iam_instance_profile  = "${aws_iam_instance_profile.ec2_s3_profile.name}"

  tags = {
    Name = "Excercise 4.1"
    Owner = "Goutam"
    Project = "Sys-Ops learning"

  }
}

resource "aws_security_group" "gou_sec_grp" {
  name        = "CertBookL"
  description = "Allow inbound traffic"  

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["157.97.119.170/32"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]    
  }

}

resource "aws_eip" "lb" {
  instance = "${aws_instance.ec2_instance.id}"
  vpc      = true
}

resource "aws_iam_instance_profile" "ec2_s3_profile" {      
    name  = "ec2_s3_profile"                         
    role = "${aws_iam_role.bastionhost.name}"
}